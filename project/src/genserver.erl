%%%-------------------------------------------------------------------
%%% @author user
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. Июль 2016 19:38
%%%-------------------------------------------------------------------
-module(genserver).

%% API
-export(
  [ stat rt/0,
    loop/1,
    add_item/2,
    remove_item/2,
    show_items/1,
    stop/1 ]
).

start() ->
  io:format("Start Server: ~p~n", [self()]),
  InitialState = [],
  spawn(?MODULE, loop, [InitialState]).

add_item(Pid, Item) ->
  Ref = make_ref(),
  Pid ! {add, self(), Ref, Item},
  receive
    {reply, Ref, Reply} -> Reply
  end.

remove_item(Pid, Item) ->
  Ref = make_ref(),
  Pid ! {remove, self(), Ref, Item},
  receive
    {reply, Ref, Reply} -> Reply
  end.

show_items(Pid) ->
  Ref = make_ref(),
  Pid ! {show, self(), Ref},
  receive
    {reply, Ref, Reply} -> Reply
  end.

stop(Pid) ->
  Pid ! stop,
  ok.


loop(State) ->
  receive
    {add, Pid, Ref, Item} ->
      NewState = [Item | State],
      Pid ! {reply, Ref, ok},
      ?MODULE:loop(NewState);

    {remove, Pid, Ref, Item} ->
      {Reply, NewState} = case lists:member(Item, State) of
                   true -> lists:delete(Item, State);
                   false -> {{error, not_exist}, State}
                 end,

    Pid ! {reply, Ref, Reply},
    ?MODULE:loop(NewState);

    {show, Pid, Ref} ->
      Pid ! {reply, Ref, State},
      ?MODULE:loop(State);

    stop -> ok;

    _ -> ?MODULE:loop(State)
  end.